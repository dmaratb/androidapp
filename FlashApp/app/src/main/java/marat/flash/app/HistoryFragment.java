package marat.flash.app;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HistoryFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HistoryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HistoryFragment extends Fragment {

    public Pair pair1;
    public Pair pair2;
    public Pair pair3;
    public Pair pair4;
    public Pair pair5;

    private OnFragmentInteractionListener mListener;
    private EditText editText1;
    private EditText editText2;
    private EditText editText3;
    private EditText editText4;
    private EditText editText5;

    private Button textBtn1;
    private Button textBtn2;
    private Button textBtn3;
    private Button textBtn4;
    private Button textBtn5;

    private Button save1;
    private Button save2;
    private Button save3;
    private Button save4;
    private Button save5;

    private Button load1;
    private Button load2;
    private Button load3;
    private Button load4;
    private Button load5;

    private String lastInput;

    public HistoryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HistoryFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HistoryFragment newInstance() {
        HistoryFragment fragment = new HistoryFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.fragment_history, container, false);
        ImageButton closeBtn = fragmentView.findViewById(R.id.close_fragment_btn);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.hideHistory("");
            }
        });

        textBtn1 = fragmentView.findViewById(R.id.slot_1_text_btn);
        textBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDefaults(1);
                editText1.requestFocus();
                save1.setEnabled(true);
                textBtn1.setVisibility(View.INVISIBLE);
            }
        });
        editText1 = fragmentView.findViewById(R.id.slot_1_text);


        textBtn2 = fragmentView.findViewById(R.id.slot_2_text_btn);
        textBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDefaults(2);
                editText2.requestFocus();
                save2.setEnabled(true);
                textBtn2.setVisibility(View.INVISIBLE);
            }
        });
        editText2 = fragmentView.findViewById(R.id.slot_2_text);


        textBtn3 = fragmentView.findViewById(R.id.slot_3_text_btn);
        textBtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDefaults(3);
                editText3.requestFocus();
                save3.setEnabled(true);
                textBtn3.setVisibility(View.INVISIBLE);
            }
        });
        editText3 = fragmentView.findViewById(R.id.slot_3_text);


        textBtn4 = fragmentView.findViewById(R.id.slot_4_text_btn);
        textBtn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDefaults(4);
                editText4.requestFocus();
                save4.setEnabled(true);
                textBtn4.setVisibility(View.INVISIBLE);
            }
        });
        editText4 = fragmentView.findViewById(R.id.slot_4_text);


        textBtn5 = fragmentView.findViewById(R.id.slot_5_text_btn);
        textBtn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDefaults(5);
                editText5.requestFocus();
                save5.setEnabled(true);
                textBtn5.setVisibility(View.INVISIBLE);
            }
        });
        editText5 = fragmentView.findViewById(R.id.slot_5_text);

        save1 = fragmentView.findViewById(R.id.slot_1_save);
        save1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText1.getText().toString().length() > 0) {
                    pair1 = new Pair(lastInput, editText1.getText().toString());
                    mListener.savePair(pair1, 1);
                    save1.setEnabled(false);
                } else {
                    Toast.makeText(getActivity(), "missing description", Toast.LENGTH_LONG).show();
                }
            }
        });

        save2 = fragmentView.findViewById(R.id.slot_2_save);
        save2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText2.getText().toString().length() > 0) {
                    pair2 = new Pair(lastInput, editText2.getText().toString());
                    mListener.savePair(pair2, 2);
                    save2.setEnabled(false);
                } else {
                    Toast.makeText(getActivity(), "missing description", Toast.LENGTH_LONG).show();
                }
            }
        });

        save3 = fragmentView.findViewById(R.id.slot_3_save);
        save3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText3.getText().toString().length() > 0) {
                    pair3 = new Pair(lastInput, editText3.getText().toString());
                    mListener.savePair(pair3, 3);
                    save3.setEnabled(false);
                } else {
                    Toast.makeText(getActivity(), "missing description", Toast.LENGTH_LONG).show();
                }
            }
        });

        save4 = fragmentView.findViewById(R.id.slot_4_save);
        save4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText4.getText().toString().length() > 0) {
                    pair4 = new Pair(lastInput, editText4.getText().toString());
                    mListener.savePair(pair4, 4);
                    save4.setEnabled(false);
                } else {
                    Toast.makeText(getActivity(), "missing description", Toast.LENGTH_LONG).show();
                }
            }
        });

        save5 = fragmentView.findViewById(R.id.slot_5_save);
        save5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText5.getText().toString().length() > 0) {
                    pair5 = new Pair(lastInput, editText5.getText().toString());
                    mListener.savePair(pair5, 5);
                    save5.setEnabled(false);
                } else {
                    Toast.makeText(getActivity(), "missing description", Toast.LENGTH_LONG).show();
                }
            }
        });

        load1 = fragmentView.findViewById(R.id.slot_1_load);
        load1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pair1 != null) {
                    mListener.hideHistory(pair1.code);
                }
            }
        });

        load2 = fragmentView.findViewById(R.id.slot_2_load);
        load2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pair2 != null) {
                    mListener.hideHistory(pair2.code);
                }
            }
        });

        load3 = fragmentView.findViewById(R.id.slot_3_load);
        load3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pair3 != null) {
                    mListener.hideHistory(pair3.code);
                }
            }
        });

        load4 = fragmentView.findViewById(R.id.slot_4_load);
        load4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pair4 != null) {
                    mListener.hideHistory(pair4.code);
                }
            }
        });

        load5 = fragmentView.findViewById(R.id.slot_5_load);
        load5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pair5 != null) {
                    mListener.hideHistory(pair5.code);
                }
            }
        });

        return fragmentView;
    }

    private void resetDefaults(int exclude) {
        // save buttons
        save1.setEnabled(false);
        save2.setEnabled(false);
        save3.setEnabled(false);
        save4.setEnabled(false);
        save5.setEnabled(false);

        textBtn1.setVisibility(View.VISIBLE);
        textBtn2.setVisibility(View.VISIBLE);
        textBtn3.setVisibility(View.VISIBLE);
        textBtn4.setVisibility(View.VISIBLE);
        textBtn5.setVisibility(View.VISIBLE);

        // text
        if (exclude != 1) {
            if (pair1 != null) {
                editText1.setText(pair1.text);
                load1.setEnabled(true);
            } else {
                editText1.setText(null);
                load1.setEnabled(false);
            }
        }

        if (exclude != 2) {
            if (pair2 != null) {
                editText2.setText(pair2.text);
                load2.setEnabled(true);
            } else {
                editText2.setText(null);
                load2.setEnabled(false);
            }
        }

        if (exclude != 3) {
            if (pair3 != null) {
                editText3.setText(pair3.text);
                load3.setEnabled(true);
            } else {
                editText3.setText(null);
                load3.setEnabled(false);
            }
        }

        if (exclude != 4) {
            if (pair4 != null) {
                editText4.setText(pair4.text);
                load4.setEnabled(true);
            } else {
                editText4.setText(null);
                load4.setEnabled(false);
            }
        }

        if (exclude != 5) {
            if (pair5 != null) {
                editText5.setText(pair5.text);
                load5.setEnabled(true);
            } else {
                editText5.setText(null);
                load5.setEnabled(false);
            }
        }
    }

    public void updateUI(String newInput) {

        lastInput = newInput;

        if (pair1 != null) {
            editText1.setText(pair1.text);
            load1.setEnabled(true);
        } else {
            editText1.setText(null);
            load1.setEnabled(false);
        }

        if (pair2 != null) {
            editText2.setText(pair2.text);
            load2.setEnabled(true);
        } else {
            editText2.setText(null);
            load2.setEnabled(false);
        }

        if (pair3 != null) {
            editText3.setText(pair3.text);
            load3.setEnabled(true);
        } else {
            editText3.setText(null);
            load3.setEnabled(false);
        }

        if (pair4 != null) {
            editText4.setText(pair4.text);
            load4.setEnabled(true);
        } else {
            editText4.setText(null);
            load4.setEnabled(false);
        }

        if (pair5 != null) {
            editText5.setText(pair5.text);
            load5.setEnabled(true);
        } else {
            editText5.setText(null);
            load5.setEnabled(false);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);

        void hideHistory(String newInput);

        void savePair(Pair newPair, int index);
    }
}
