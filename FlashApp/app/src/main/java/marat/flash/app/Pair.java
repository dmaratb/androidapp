package marat.flash.app;

import androidx.annotation.NonNull;

public class Pair {

    public String code;
    public String text;

    public Pair(String code, String text) {
        this.code = code;
        this.text = text;
    }

    @NonNull
    @Override
    public String toString() {
        return this.text;
    }
}
