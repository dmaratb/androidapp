package marat.flash.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements HistoryFragment.OnFragmentInteractionListener {

    Handler flashTaskHandler;
    private List<String> recentList = new ArrayList<>(5);
    private ConstraintLayout recentContainer;
    private TextView recent_1_text;
    private TextView recent_2_text;
    private TextView recent_3_text;
    private TextView recent_4_text;
    private TextView recent_5_text;

    private TableLayout settingsContainer;
    private TextView durationText;
    private Button increaseBtn;
    private Button decreaseBtn;
    private int unitDuration = 200;

    private EditText mainEditText;
    private TextView durationLabel;
    private FragmentManager fragmentManager;
    private HistoryFragment historyFragment;

    private CameraManager cameraManager;
    private String cameraId;
    private String flashSequence;


    Runnable flashTask = new Runnable() {
        @Override
        public void run() {

            if (flashSequence.length() > 0) {

                switch (flashSequence.charAt(0)) {
                    case '0':
                        try {
                            cameraManager.setTorchMode(cameraId, false);
                        } catch (CameraAccessException e) {
                            e.printStackTrace();
                        }
                        break;
                    case '1':
                        try {
                            cameraManager.setTorchMode(cameraId, true);
                        } catch (CameraAccessException e) {
                            e.printStackTrace();
                        }
                        break;
                }

                flashSequence = flashSequence.substring(1);
                flashTaskHandler.postDelayed(flashTask, unitDuration);
            } else {
                try {
                    cameraManager.setTorchMode(cameraId, false);
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }

                Toast.makeText(MainActivity.this, "DONE !!!", Toast.LENGTH_SHORT).show();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // settings
        initSettings();

        // recent
        initRecent();

        // edit text
        initMainEditText();

        // toggle
        initToggleBtn();

        // camera
        initCamera();

        // play
        initPlayBtn();

        // save
        initSaveBtn();
    }

    void initSettings() {
        final SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        unitDuration = preferences.getInt("unitDuration", 200);

        settingsContainer = findViewById(R.id.settings_container);
        settingsContainer.setVisibility(View.GONE);

        ImageButton settingsBtn = findViewById(R.id.settings_btn);
        settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsContainer.setVisibility(View.VISIBLE);
            }
        });

        ImageButton closeBtn = findViewById(R.id.close_btn);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsContainer.setVisibility(View.GONE);
            }
        });
        durationText = findViewById(R.id.duration_text);
        durationText.setText(String.format("%.1f", (float) unitDuration / 1000));

        increaseBtn = findViewById(R.id.increase_btn);
        increaseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (unitDuration < 1000) {
                    unitDuration += 100;
                    durationText.setText(String.format("%.1f", (float) unitDuration / 1000));
                    SharedPreferences.Editor prefEditor = preferences.edit();
                    prefEditor.putInt("unitDuration", unitDuration);
                    prefEditor.commit();

                } else {

                }
            }
        });

        decreaseBtn = findViewById(R.id.decrease_btn);
        decreaseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (unitDuration > 0) {
                    unitDuration -= 100;
                    durationText.setText(String.format("%.1f", (float) unitDuration / 1000));
                    SharedPreferences.Editor prefEditor = preferences.edit();
                    prefEditor.putInt("unitDuration", unitDuration);
                    prefEditor.commit();

                } else {

                }
            }
        });
    }

    void initRecent() {

        recentContainer = findViewById(R.id.recent_container);
        recentContainer.setVisibility(View.GONE);

        recent_1_text = findViewById(R.id.recent1);
        recent_2_text = findViewById(R.id.recent2);
        recent_3_text = findViewById(R.id.recent3);
        recent_4_text = findViewById(R.id.recent4);
        recent_5_text = findViewById(R.id.recent5);

        recent_1_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recent_1_text.getText().toString().length() > 0) {
                    mainEditText.setText(recent_1_text.getText());
                    recentContainer.setVisibility(View.GONE);
                }
            }
        });

        recent_2_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recent_2_text.getText().toString().length() > 0) {
                    mainEditText.setText(recent_2_text.getText());
                    recentContainer.setVisibility(View.GONE);
                }
            }
        });

        recent_3_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recent_3_text.getText().toString().length() > 0) {
                    mainEditText.setText(recent_3_text.getText());
                    recentContainer.setVisibility(View.GONE);
                }
            }
        });

        recent_4_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recent_4_text.getText().toString().length() > 0) {
                    mainEditText.setText(recent_4_text.getText());
                    recentContainer.setVisibility(View.GONE);
                }
            }
        });

        recent_5_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recent_5_text.getText().toString().length() > 0) {
                    mainEditText.setText(recent_5_text.getText());
                    recentContainer.setVisibility(View.GONE);
                }
            }
        });

        ImageButton recentBtn = findViewById(R.id.recent_btn);
        recentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (recentContainer.getVisibility() == View.VISIBLE) {
                    recentContainer.setVisibility(View.GONE);
                } else {
                    recentContainer.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    void initMainEditText() {
        durationLabel = findViewById(R.id.length_label);

        mainEditText = findViewById(R.id.main_text_input);
        mainEditText.setRawInputType(Configuration.KEYBOARD_12KEY);
        mainEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                durationLabel.setText(String.format("%.1f sec.", (float)unitDuration * s.length() / 1000));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ImageButton clearBtn = findViewById(R.id.clear_btn);
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainEditText.setText("");
            }
        });
    }

    void initToggleBtn() {
        ImageButton toggleBtn = findViewById(R.id.toggle_text_btn);
        toggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainEditText.getMaxLines() > 1) {
                    mainEditText.setMaxLines(1);
                } else {
                    mainEditText.setMaxLines(Integer.MAX_VALUE);
                }
            }
        });
    }

    void initCamera() {

        boolean flashAvailable = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        if (flashAvailable) {

            cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            try {
                if (cameraManager != null)
                    cameraId = cameraManager.getCameraIdList()[0];
            } catch (CameraAccessException e) {
                e.printStackTrace();
                Toast.makeText(MainActivity.this, "camera not available", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(MainActivity.this, "flash not available", Toast.LENGTH_SHORT).show();
        }
    }

    void initPlayBtn() {
        flashTaskHandler = new Handler();

        ImageButton playBtn = findViewById(R.id.start_btn);
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flashSequence = mainEditText.getText().toString();

                // update recent
                recentList.add(flashSequence);
                if (recentList.size() > 5) {
                    recentList.remove(0);
                }

                for (int index = 0; index < recentList.size(); index++) {
                    String nextText = recentList.get(index);
                    switch (index) {
                        case 0:
                            recent_1_text.setText(nextText);
                            break;
                        case 1:
                            recent_2_text.setText(nextText);
                            break;
                        case 2:
                            recent_3_text.setText(nextText);
                            break;
                        case 3:
                            recent_4_text.setText(nextText);
                            break;
                        case 4:
                            recent_5_text.setText(nextText);
                            break;
                    }
                }

                // play sequence
                if (cameraId != null) {
                    Toast.makeText(MainActivity.this, "STARTED...", Toast.LENGTH_SHORT).show();
                    startPlaying();
                } else {
                    Toast.makeText(MainActivity.this, "camera not available", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void startPlaying() {
        try {
            cameraManager.setTorchMode(cameraId, false);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        flashTask.run();
    }

    void initSaveBtn() {

        Gson gson = new Gson();
        fragmentManager = getSupportFragmentManager();
        historyFragment = (HistoryFragment) fragmentManager.findFragmentById(R.id.history_fragment);
        FragmentTransaction ft = fragmentManager.beginTransaction();

        ft.hide(historyFragment);
        ft.commit();

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        String json = preferences.getString("pair1", "");
        if (json.length() > 0) {
            Pair pair1 = gson.fromJson(json, Pair.class);
            historyFragment.pair1 = pair1;
        } else {
            historyFragment.pair1 = null;
        }

        json = preferences.getString("pair2", "");
        if (json.length() > 0) {
            Pair pair2 = gson.fromJson(json, Pair.class);
            historyFragment.pair2 = pair2;
        } else {
            historyFragment.pair2 = null;
        }

        json = preferences.getString("pair3", "");
        if (json.length() > 0) {
            Pair pair3 = gson.fromJson(json, Pair.class);
            historyFragment.pair3 = pair3;
        } else {
            historyFragment.pair3 = null;
        }

        json = preferences.getString("pair4", "");
        if (json.length() > 0) {
            Pair pair4 = gson.fromJson(json, Pair.class);
            historyFragment.pair4 = pair4;
        } else {
            historyFragment.pair4 = null;
        }

        json = preferences.getString("pair5", "");
        if (json.length() > 0) {
            Pair pair5 = gson.fromJson(json, Pair.class);
            historyFragment.pair5 = pair5;
        } else {
            historyFragment.pair5 = null;
        }

        ImageButton saveBtn = findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mainEditText.clearFocus();

                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.show(historyFragment);
                historyFragment.updateUI(mainEditText.getText().toString());

                ft.commit();
            }
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void hideHistory(String newInput) {
        if (newInput.length() > 0) {
            mainEditText.setText(newInput);
        }

        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.hide(historyFragment);
        ft.commit();
    }

    @Override
    public void savePair(Pair newPair, int index) {

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(newPair);
        String key = "pair" + index;
        prefEditor.putString(key, json);

        if (prefEditor.commit()) {
            Toast.makeText(MainActivity.this, "sequence saved", Toast.LENGTH_SHORT).show();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.hide(historyFragment);
            ft.commit();
        } else {
            Toast.makeText(MainActivity.this, "failed to save", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {

        if (settingsContainer.getVisibility() == View.VISIBLE) {
            settingsContainer.setVisibility(View.GONE);
            return;
        }

        if (recentContainer.getVisibility() == View.VISIBLE) {
            recentContainer.setVisibility(View.GONE);
            return;
        }

        if (historyFragment.isHidden()) {
            super.onBackPressed();
        } else {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.hide(historyFragment);
            ft.commit();
        }
    }
}
